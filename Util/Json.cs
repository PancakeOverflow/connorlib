﻿using System;
using System.Collections;
using System.IO;
using System.Text.Json;


namespace ConnorLib.Util
{
    /// <summary>
    /// Helpers for working with JSON.
    /// </summary>
    public class Json
    {
        /// <summary>
        /// Get a strongly-typed representation of a .json file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static T LoadFromFile<T>(string filePath)
        {
            using StreamReader reader = new StreamReader(filePath);
            string json = reader.ReadToEnd();
            T result = JsonSerializer.Deserialize<T>(json);
            return result;
        }

        /// <summary>
        /// Convert dynamic/stringified JSON into a strongly-typed object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonObject"></param>
        /// <returns></returns>
        public static T Deserialize<T>(object jsonObject)
            => JsonSerializer.Deserialize<T>(Convert.ToString(jsonObject));

        /// <summary>
        /// Convert a strongly-typed object into a JSON string.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static string Serialize<T>(T instance) => JsonSerializer.Serialize(instance);

        /// <summary>
        /// Converts a JSON string into a .NET JsonDocument
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static JsonElement GetRootJson(string json) 
            => JsonDocument.Parse(json).RootElement;

    }
}
