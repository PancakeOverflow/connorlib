﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Http;

namespace ConnorLib.Util
{
    /// <summary>
    /// Provides layers over the HttpClient class.
    /// </summary>
    public class RESTful
    {
        /// <summary>
        /// An HttpClient instance.
        /// </summary>
        public static readonly HttpClient Client = new HttpClient();

        /// <summary>
        /// The root URI to the EfinityAPI (relative to the current environment).
        /// </summary>
#if DEBUG
        public const string EFINITY_API_DOMAIN = "https://localhost:44355";
#else
        public const string EFINITY_API_DOMAIN = "https://api.efinitytech.com";
#endif


        /// <summary>
        /// Sends a POST request to the specified URI.
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="body">The POST request body.</param>
        /// <param name="jwt">Optional parameter for authentication via JWT token.</param>
        /// <returns></returns>
        public static async Task<string> Post(string uri, dynamic body, string jwt = default)
        {
            dynamic requestBody = Util.Json.Serialize(body);
            using HttpRequestMessage newRequest = CreateRequest("POST", uri, requestBody);
            if (jwt != default)
            {
                newRequest.Headers.Authorization = CreateTokenHeader(jwt);
            }
            try
            {
                HttpResponseMessage response = await Client.SendAsync(newRequest);
                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Sends a GET request to the specified URI.
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="query">The GET request query string.</param>
        /// <param name="jwt">Optional parameter for authentication via JWT token.</param>
        /// <returns></returns>
        public static async Task<string> Get(string uri, dynamic query, string jwt = default)
        {
            using HttpRequestMessage newRequest = CreateRequest("GET", $"{uri}{ToQueryString(query)}");
            if (jwt != default)
            {
                newRequest.Headers.Authorization = CreateTokenHeader(jwt);
            }
            HttpResponseMessage response = await Client.SendAsync(newRequest);
            return await response.Content.ReadAsStringAsync();
        }

        /// <summary>
        /// Forwards an <see cref="HttpRequest"/> to the specified destination.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> SendRequest(HttpRequest request, string destination)
        {
            string requestContent;
            using (Stream receiveStream = request.Body)
            {
                using StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                requestContent = await readStream.ReadToEndAsync();
            }

            string cType = request.ContentType != null
                ? request.ContentType.Split(';')[0]
                : "application/json";
            using HttpRequestMessage newRequest
                = CreateRequest(request.Method, destination, requestContent, cType);
            return await Client.SendAsync(newRequest);
        }

        /// <summary>
        /// Creates an AuthenticationHeaderValue for JWT token (bearer) type auth.
        /// </summary>
        /// <param name="jwt"></param>
        /// <returns></returns>
        public static AuthenticationHeaderValue CreateTokenHeader(string jwt)
            => new AuthenticationHeaderValue("Bearer", jwt);

        /// <summary>
        /// Convert an object to a query string.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToQueryString(object obj)
        {
            StringBuilder result = new StringBuilder("?");
            foreach (System.Reflection.PropertyInfo prop in obj.GetType().GetProperties())
            {
                switch (prop.GetValue(obj))
                {
                    case IEnumerable<object> list:
                        foreach (object i in list)
                        {
                            result.Append($"{prop.Name}={i}&");
                        }
                        break;
                    case object val:
                        result.Append($"{prop.Name}={val}&");
                        break;
                    default:
                        break;
                }
            }
            return result.ToString().TrimEnd('&');
        }

        /// <summary>
        /// Creates a new HttpRequestMessage.
        /// </summary>
        /// <param name="method"></param>
        /// <param name="destination"></param>
        /// <param name="body"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        public static HttpRequestMessage CreateRequest(string method,
            string destination,
            string body = "",
            string contentType = "application/json")
        => new HttpRequestMessage(new HttpMethod(method), destination)
        {
            Content = new StringContent(body, Encoding.UTF8, contentType ?? "application/json")
        };

        /// <summary>
        /// Creates a new HttpRequestMessage from an HttpRequest.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="destination"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public static HttpRequestMessage CreateRequest(HttpRequest request,
            string destination,
            string content)
            => new HttpRequestMessage(new HttpMethod(request.Method), destination)
            {
                Content = new StringContent(content, Encoding.UTF8, request.ContentType)
            };

        /// <summary>
        /// Get the content type from a request.
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        public static string GetContentType(HttpResponseMessage response)
            => response.Content.Headers.ContentType.MediaType;

        /// <summary>
        /// Tries to convert a string response body into deserialized JSON or XML, depending on the 
        /// content type. Otherwise, returns the original string.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="responseBody"></param>
        /// <returns></returns>
        public static dynamic TryDeserializeBody(string type, string responseBody) => type switch
        {
            MediaTypeNames.Application.Json => Util.Json.Deserialize<dynamic>(responseBody),
            MediaTypeNames.Application.Xml
                => new XmlSerializer(typeof(string)).Deserialize(new StringReader(responseBody)),
            _ => responseBody,
        };

        public static string TryGetCookie(HttpContext context, string name) => context.Request.Cookies.ContainsKey(name)
            ? context.Request.Cookies.FirstOrDefault(c => c.Key == name).Value
            : "";
    }

}
