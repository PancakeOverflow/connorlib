﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConnorLib.Util
{
    public class Conversion
    {

        public static Encoding GetEncoding() => Encoding.UTF8;

        /// <summary>
        /// Converts a base64 <c>string</c> to a <c>byte[]</c>.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static byte[] String64ToBytes(string text) => Convert.FromBase64String(text);

        /// <summary>
        /// Converts a <c>string</c> to a <c>byte[]</c>.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static byte[] StringToBytes(string text) => GetEncoding().GetBytes(text);

        /// <summary>
        /// Converts a <c>byte[]</c> to a base64 <c>string</c>.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string BytesToString64(byte[] bytes) => Convert.ToBase64String(bytes);

        /// <summary>
        /// Converts a <c>byte[]</c> to a <c>string</c>.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string StringToBytes(byte[] bytes) => GetEncoding().GetString(bytes);

        /// <summary>
        /// Returns the offset in days from the current UTC standard time.
        /// </summary>
        /// <param name="days"></param>
        /// <returns></returns>
        public static DateTime CalculateDaysFromNow(int days) => DateTime.UtcNow.AddDays(days);

        /// <summary>
        /// Gets the current UTC time.
        /// </summary>
        /// <returns></returns>
        public static DateTime Now() => DateTime.UtcNow;

        /// <summary>
        /// Returns a dictionary using parameter names as keys, mapped to the values in the passed 
        /// instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static Dictionary<string, object> ObjectToDictionary<T>(T obj)
        {
            var result = new Dictionary<string, object>();
            foreach (var property in obj.GetType().GetProperties())
            {
                result.Add(property.Name, property.GetValue(obj));
            }
            return result;
        }

        /// <summary>
        /// Convert a string into kabab-case.
        /// </summary>
        /// <example>
        /// "myCustomParameterName" => "my-custom-parameter-name"
        /// </example>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string Kabab(string str) 
            => Regex.Replace(Regex.Replace(str, @"([A-Z]+)", "-$1"), @"\W+", "-").Trim('-');

        /// <summary>
        /// This kinda defeats the purpose of a secure string, but whatever.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static SecureString SecureString(string s)
            => new System.Net.NetworkCredential("", s).SecurePassword;

    }
}
