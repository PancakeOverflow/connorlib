﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;
using System.Text.RegularExpressions;
#if DEBUG
using System.Diagnostics;
#endif

namespace ConnorLib.Util
{
    public class Debugger
    {
        private static readonly Dictionary<string, StringBuilder> _builders = new Dictionary<string, StringBuilder>();

        public static void Info(HttpContext context, params object[] items) =>
            Info(context.TraceIdentifier, items);

        public static void Warn(HttpContext context, params object[] items) =>
            Warn(context.TraceIdentifier, items);

        public static void Error(HttpContext context, params object[] items) =>
            Error(context.TraceIdentifier, items);

        public static void Debug(HttpContext context, params object[] items)
        {
#if DEBUG
            AddLine(context.TraceIdentifier, "DBUG  > ", items);
            StackFrame stack = new StackFrame(1, true);
            string filename = Regex.Replace(stack.GetFileName() ?? "[unknown]", @".*((?:\w+[/\\]?){,3})$", "$1");
            AddLine(context.TraceIdentifier, "TRAC  > ", $"{stack.GetFileName()}:{stack.GetFileLineNumber()}'{stack.GetMethod()}'");
#endif
        }

        public static void Info(string key, params object[] items)
        {
            AddLine(key, "INFO  > ", items);
        }

        public static void Warn(string key, params object[] items)
        {
            AddLine(key, "WARN  > ", items);
        }

        public static void Error(string key, params object[] items)
        {
            AddLine(key, "ERR!  > ", items);
        }

        public static void Debug(string key, params object[] items)
        {
#if DEBUG
            AddLine(key, "DBUG  > ", items);
            StackFrame sf = new StackFrame(1, true);
            string input = sf.GetFileName() ?? "[unknown]";
            string filename = Regex.Replace(input, @".*?((?:\w+[/\\]){0,3})([^\\/]+)$", "$1$2");
            string trace = $"{sf.GetFileName()}:{sf.GetFileLineNumber()}'{sf.GetMethod()}'";
            AddLine(key, "TRAC  > ", trace);
#endif
        }

        /// <summary>
        /// End a logging session and return the log contents.
        /// </summary>
        /// <param name="key"></param>
        public static string ExtractLogs(string key)
        {
            try
            {
                StringBuilder builder = GetBuilder(key);
                string result = builder.ToString();
                builder.Clear();
                _builders.Remove(key);
                return result;
            }
            catch (Exception e)
            {
                FileSystem.WriteTimestampedFile(FileSystem.Here(), "meta.log", e.ToString());
            }
            return string.Empty;
        }

        /// <summary>
        /// Append a line to a builder
        /// </summary>
        /// <param name="key"></param>
        /// <param name="statements"></param>
        private static void AddLine(string key, string prefix, params object[] statements)
        {
            try
            {

                StringBuilder builder = GetBuilder(key);
                builder.Append(prefix);
                statements.ToList().ForEach(s =>
                {
                    builder.Append(@$"{Regex.Replace(s.ToString(), @"[\n\r]+", " ")} ");
                });
                builder.AppendLine();
            }
            catch (Exception e)
            {
                FileSystem.WriteTimestampedFile(FileSystem.Here(), "meta.log", e.ToString());
            }
        }

        /// <summary>
        /// Gets a new or existing builder by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private static StringBuilder GetBuilder(string key)
        {
            if (_builders.Keys.Contains(key))
            {
                return _builders[key];
            }
            StringBuilder builder = new StringBuilder();
            _builders.Add(key, builder);
            return builder;
        }

    }
}
