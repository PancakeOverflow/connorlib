﻿using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;

namespace ConnorLib.Services
{
    public interface ITokenService
    {
        /// <summary>
        /// Convert a encoded token string into an object property-to-claim mapping.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="token">The serialized token, e.g. a JWT.</param>
        /// <returns></returns>
        public T DecodeAndMap<T>(string token) where T : new();

        /// <summary>
        /// Attempt to retrieve a claim from a token. 
        /// </summary>
        /// <param name="token">The serialized token, e.g. a JWT.</param>
        /// <param name="type">The type/name of the claim</param>
        /// <param name="claim"></param>
        /// <returns>True, if the claim may be returned.</returns>
        public bool TryGetClaim(string token, string type, out Claim claim);

        /// <summary>
        /// Verify that a token has a claim and that it is equal to the specified expected value.
        /// </summary>
        /// <param name="token">The serialized token, e.g. a JWT.</param>
        /// <param name="type">The type/name of the claim</param>
        /// <param name="expectedValue">The value the claim should produce to be valid.</param>
        /// <returns>True, if the claim is valid.</returns>
        public bool VerifyClaim<T>(string token, string type, T expectedValue);

        /// <summary>
        /// Returns an encoded token, built from the passed-in list of claims and the configured claims 
        /// configured in the appsettings.
        /// </summary>
        /// <param name="claims"></param>
        /// <returns></returns>
        public string CreateEncodedToken<T>(string name, T payload);

        /// <summary>
        /// Returns true if the token is valid.
        /// </summary>
        /// <param name="token">The serialized token, e.g. a JWT.</param>
        /// <param name="principal">The claims within the valid token.</param>
        /// <param name="validated">The valid security token.</param>
        /// <returns></returns>
        public bool ValidateToken(string token, out ClaimsPrincipal principal, out SecurityToken validated);

        /// <summary>
        /// Returns true if the token is valid.
        /// </summary>
        /// <param name="token">The serialized token, e.g. a JWT.</param>
        /// <returns></returns>
        public bool ValidateToken(string token);
    }
}
