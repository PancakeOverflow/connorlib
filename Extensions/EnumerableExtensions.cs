﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConnorLib.Extensions
{
    /// <summary>
    /// Adds mappings to <see cref="Util.Iterable"/> as extension methods to implementations of IEnumerable.
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Perform transformations on every member of an iterable, resulting in a one-to-one 
        /// mapping of the original list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="Tout"></typeparam>
        /// <param name="source"></param>
        /// <param name="func"></param>
        /// <returns>A one-to-one mapping of the original list</returns>
        public static IEnumerable<Tout> Map<T, Tout>(this IEnumerable<T> source, Func<T, Tout> func) => Util.Iterable.Map(source, func);

        /// <summary>
        /// Similar to <see cref="Map{T, Tout}(IEnumerable{T}, Func{T, Tout})"/>, Mash performs
        /// transformations on the <paramref name="source"/> using every item in the 
        /// <paramref name="foreign"/> as input.
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="Tout"></typeparam>
        /// <param name="source"></param>
        /// <param name="foreign"></param>
        /// <param name="func"></param>
        /// <returns>A one-to-one mapping of the original list.</returns>
        public static IEnumerable<Tout> Mash<T1, T2, Tout>(this IEnumerable<T1> source, IEnumerable<T2> foreign, Func<T1, T2, Tout> func)
            => Util.Iterable.Mash(source, foreign, func);

        /// <summary>
        /// Iterates through every member of an iterable and returns a single member comprised of
        /// the combined evaluation and transformation of each item in the original list: each 
        /// iteration takes the result of the previous as input and performs transformations using
        /// the current element in the iterable.    
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="Tout"></typeparam>
        /// <param name="list"></param>
        /// <param name="func"></param>
        /// <param name="initial"></param>
        /// <returns></returns>
        public static Tout Reduce<T, Tout>(this IEnumerable<T> list, Func<Tout, T, Tout> func, Tout initial = default)
            => Util.Iterable.Reduce(list, func, initial);

        /// <summary>
        /// Like <see cref="Mash"/>, but returns only the elements whose <paramref name="compare"/>
        /// function execution return true. 
        /// <para>
        /// All elements from both the <paramref name="source"/> and <paramref name="join"/> are 
        /// compared. When a comparision returns true, the item from the <paramref name="source"/>
        /// list will be included in the result.
        /// </para>
        /// </summary>
        /// <typeparam name="Tout"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="join"></param>
        /// <param name="compare"></param>
        /// <returns></returns>
        public static IEnumerable<Tout> Intersect<Tout, T>(this IEnumerable<Tout> source, IEnumerable<T> join, Func<Tout, T, bool> compare)
            => Util.Iterable.Intersect(source, join, compare);
    }
}
