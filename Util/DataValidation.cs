﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace ConnorLib.Util
{
    public class DataValidation
    {
        /// <summary>
        /// Checks to see if a property name is in an object.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsMemberOf(string propertyName, Type type)
            => type.GetProperty(propertyName) != null;

        /// <summary>
        /// Checks to see if a property name is in an object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static bool IsMemberOf<T>(string propertyName)
            => typeof(T).GetProperty(propertyName) != null;

        /// <summary>
        /// Look, I'm aware that you shouldn't use Regex to verify email addresses because it can't 
        /// possibly match the intricacies of what's permitted by the Internet Message Format RFC. 
        /// However, that RFC sucks butt; if a user manages to create an email address that fails 
        /// against this validator, we don't want it in our system anyway.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool LooksLikeEmail(string email)
            => Regex.IsMatch(email, @".+@.+\..+");

        /// <summary>
        /// Validates a string to see if it is a valid Parameter name for a C# object.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool IsValidParameterKey(string key) => Regex.IsMatch(key, @"^[a-zA-Z_]\w+$");
    }
}
