﻿namespace ConnorLib.Services
{
    public interface ICryptoService
    {
        /// <summary>
        /// Test for equivalency between two hash values.
        /// </summary>
        /// <param name="theirs"></param>
        /// <param name="ours"></param>
        /// <returns></returns>
        public bool CompareHashes(byte[] theirs, byte[] ours);

        /// <summary>
        /// Generate a cryptographically strong sequence of random values.
        /// </summary>
        /// <returns></returns>
        public byte[] GenerateRandomSalt();

        /// <summary>
        /// Get a hashed, salted password
        /// </summary>
        /// <param name="password"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public byte[] HashPassword(string password, byte[] salt);

        /// <summary>
        /// Iteratively hashes a password.
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public byte[] ComputeHash(string password);

        /// <summary>
        /// Encrypt a string.
        /// </summary>
        /// <param name="plain"></param>
        /// <returns></returns>
        public byte[] Encrypt(ref string plain);

        /// <summary>
        /// Decrypt a string.
        /// </summary>
        /// <param name="encrypted"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public void Decrypt(ref string encrypted);
    }
}
