﻿using Microsoft.Extensions.Configuration;

namespace ConnorLib.Services
{
    public abstract class ConfiguredService
    {
        protected readonly IConfiguration _configuration;
        public ConfiguredService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
    }
}
