﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConnorLib.Exceptions
{
    public class DuplicateKeysException : Exception
    {
        public DuplicateKeysException() { }
        public DuplicateKeysException(string message) : base(message) { }
        public DuplicateKeysException(string message, Exception inner) : base(message, inner) { }
    }
}
