﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConnorLib.Util
{
    
    /// <summary>
    /// Methods for modifying/reading IEnumerables and other iterable structures.
    /// </summary>
    public class Iterable
    {
        /// <summary>
        /// Perform transformations on every member of an iterable, resulting in a one-to-one 
        /// mapping of the original list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="Tout"></typeparam>
        /// <param name="source"></param>
        /// <param name="func"></param>
        /// <returns>A one-to-one mapping of the original list</returns>
        public static IEnumerable<Tout> Map<T, Tout>(IEnumerable<T> source, Func<T, Tout> func)
        {
            foreach (var i in source)
            {
                yield return func(i);
            }
        }

        /// <summary>
        /// Similar to <see cref="Map{T, Tout}(IEnumerable{T}, Func{T, Tout})"/>, Mash performs
        /// transformations on the <paramref name="source"/> using every item in the 
        /// <paramref name="foreign"/> as input.
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="Tout"></typeparam>
        /// <param name="source"></param>
        /// <param name="foreign"></param>
        /// <param name="func"></param>
        /// <returns>A one-to-one mapping of the original list.</returns>
        public static IEnumerable<Tout> Mash<T1, T2, Tout>(IEnumerable<T1> source, IEnumerable<T2> foreign, Func<T1, T2, Tout> func)
        {
            foreach (var o in source)
            {
                foreach (var i in foreign)
                {
                    yield return func(o, i);
                }
            }
        }

        /// <summary>
        /// Iterates through every member of an iterable and returns a single member comprised of
        /// the combined evaluation and transformation of each item in the original list: each 
        /// iteration takes the result of the previous as input and performs transformations using
        /// the current element in the iterable.    
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="Tout"></typeparam>
        /// <param name="list"></param>
        /// <param name="func"></param>
        /// <param name="initial"></param>
        /// <returns></returns>
        public static Tout Reduce<T, Tout>(IEnumerable<T> list, Func<Tout, T, Tout> func, Tout initial = default)
        {
            Tout result = initial;
            foreach (var i in list)
            {
                result = func(result, i);
            }
            return result;
        }

        /// <summary>
        /// Like <see cref="Mash"/>, but returns only the elements whose <paramref name="compare"/>
        /// function execution return true. 
        /// <para>
        /// All elements from both the <paramref name="source"/> and <paramref name="join"/> are 
        /// compared. When a comparision returns true, the item from the <paramref name="source"/>
        /// list will be included in the result.
        /// </para>
        /// </summary>
        /// <typeparam name="Tout"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="join"></param>
        /// <param name="compare"></param>
        /// <returns></returns>
        public static IEnumerable<Tout> Intersect<Tout, T>(IEnumerable<Tout> source, IEnumerable<T> join, Func<Tout, T, bool> compare)
        {
            foreach (var l in source)
            {
                foreach (var r in join)
                {
                    if (compare(l, r))
                    {
                        yield return l;
                    }
                }
            }
        }
    }

}
