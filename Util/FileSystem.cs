﻿using System;
using System.IO;

namespace ConnorLib.Util
{
    public class FileSystem
    {
        /// <summary>
        /// Saves a file to a directory. If the directory does not exist, this also creates it first.
        /// <para>All subsequent path parameters will be combined into a single path.</para>
        /// </summary>
        /// <exception cref="NullReferenceException">Thrown when the path is not provided.</exception>
        /// <param name="content"></param>
        /// <param name="path"></param>
        public static void WriteFile(string content, params string[] path)
        {
            if (path.Length == 0)
            {
                throw new NullReferenceException();
            }
            string actualPath = Path.Combine(path);
            string dir = Path.GetDirectoryName(actualPath);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            File.WriteAllText(actualPath, content);
        }

        /// <summary>
        /// Write out a time-stamped file.
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="fileName"></param>
        /// <param name="content"></param>
        /// <param name="format"></param>
        public static void WriteTimestampedFile(string dir, string fileName, string content,
            string format = "yyyy-MM-dd_HHmmss_")
        {
            string stampedName = $"{DateTime.Now.ToString(format)}{fileName}";
            WriteFile(content, dir, stampedName);
        }

        /// <summary>
        /// Reads the contents of a file. If no file exists, returns an empty string.
        /// <para>All parameters will be combined into a single path.</para>
        /// </summary>
        /// <exception cref="NullReferenceException">Thrown when the path is not provided.</exception>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ReadFile(params string[] path)
        {
            if (path.Length == 0)
            {
                throw new NullReferenceException();
            }
            if (File.Exists(Path.Combine(path)))
            {
                using StreamReader file = new StreamReader(Path.Combine(path));
                return file.ReadToEnd();
            }
            return "";
        }

        public static string Here() => Directory.GetCurrentDirectory();
    }
}
