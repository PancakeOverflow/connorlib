﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConnorLib.Util
{
    public class Randomize
    {
        public static Random Random = new Random();
        private static readonly char[] _alphabet = new char[] {'a','b','c','d','e','f','g','h','j','k','l','m','n','p','q','r','s','t','u','w','x','y','z',
            'A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','W','X','Y','Z'};

        /// <summary>
        /// Generate a random int (any positive int by default).
        /// </summary>
        /// <param name="min"></param>
        /// <returns></returns>
        public static int Integer(int min = 0, int max = int.MaxValue) => Random.Next(min, max);

        /// <summary>
        /// Generate a random Alphabetical String.
        /// </summary>
        /// <returns></returns>
        public static string Alpha(int len = 16)
        {
            string result = "";

            for (int i = 0; i < len; i++)
            {
                result += _alphabet[Random.Next(_alphabet.Length)];
            }
            return result;
        }
    }
}
